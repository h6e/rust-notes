# Cargo

## Private GitLab-Repositorien einbinden

Öffentliche Git-Repositorien können wie folgt eingebunden werden:

```toml
[dependencies]
public-repo = {git = "https://gitlab.instance.io/project/public-repo.git", tag = "v1.0.0"}
```

Um ein privates Git-Repositorium bei lokalen Builds einzubinden, muss das Repositorium über ssh geklont werden:

```toml
[dependencies]
private-repo = {git = "ssh://git@gitlab.instance.io/project/private-repo.git", tag = "v1.0.0"}
```

Damit der private ssh-Schlüssel verwendet wird, muss zudem cargo entsprechend konfiguriert werden:

```sh
mkdir .cargo
cat > .cargo/config <<EOF
[net]
git-fetch-with-cli = true
EOF
```

Für die Nutzung in Gitlab-CI-Prozessen muss die Pipeline so eingestellt werden, dass sie das CI-Access-Token beim Zugriff auf das Repositorium verwendet (das funktioniert natürlich nur, wenn das private Repo in derselben Gruppe wie der CI-Prozess liegt).

In `.gitlab-ci.yml` zu ergänzen:

```yaml
.git-access:
  before_script:
    - git config --global credential.helper store
    - echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.instance.io" > ~/.git-credentials
    - git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.instance.io".insteadOf ssh://gitlab.instance.io
```

Damit wird eine Rewrite-Regel definiert, womit [git bestimmte URLs ersetzen kann](https://git-scm.com/docs/git-config#Documentation/git-config.txt-urlltbasegtinsteadOf).


TODO: Docker / Podman-Integration (s. https://oliverjumpertz.com/blog/how-to-handle-private-gitlab-dependencies-with-cargo/)